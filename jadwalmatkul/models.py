from django.db import models

# Create your models here.
class Schedule(models.Model):
    Name = models.CharField(max_length=50)
    Lecturer = models.DateField(max_length=50)
    SKS = models.IntegerField(blank=False)
    Description = models.CharField(max_length=50)
    Semester = models.CharField(max_length=50)
    Class = models.CharField(max_length=50)