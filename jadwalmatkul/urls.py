from django.urls import path
from .views import join, delete, see

appname = 'jadwalmatkul'

urlpatterns = [
    path('', views.join, name = 'schedule'),
    path('see/delete/<int:pk>', delete, name = 'delete'),
    path('see/sched/<int:pk>', see, name='see'),
]