from django import forms

class SchedForm(forms.Form):

    Name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Activity Name',
        'required': True,
    }))

    Lecturer = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Lecturer',
        'required': True,
    }))

    SKS = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'integer',
        'placeholder': 'SKS',
        'required': True,
    }))

    Description = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Description',
        'required': True,
    }))

    Semester = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Semester',
        'required': True,
    }))
    
    Class = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Class',
        'required': True,
    }))