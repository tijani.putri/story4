from django.shortcuts import render
from .models import Schedule as jadwal
from .forms import SchedForm
# Create your views here.

def join(request):
    if request.method == "POST":
        form = SchedForm(request.POST)
        if form.is_valid():
            sched = jadwalmatkul()
            sched.Name = form.cleaned_data['Name']
            sched.Lectures = form.cleaned_data['Lecturer']
            sched.SKS = form.cleaned_data['SKS']
            sched.Description = form.cleaned_data['Description']
            sched.Semester = form.cleaned_data['Semester']
            sched.Class = form.cleaned_data['Class']
            sched.save()
        return redirect('/schedule')
    else:
        sched = jadwal.objects.all()
        form = SchedForm()
        response = {"sched":sched, 'form':form}
        return render(request, 'schedule.html', response)

def delete(request):
    if request.method == "POST":
        pk = request.POST['pk']
        jadwal.objects.get(pk=pk).delete()
        return redirect('/schedule')
    else:
        sched = jadwal.objects.all()
        form = SchedForm()
        response = {"sched": sched, 'form':form}
        return render(request, 'schedule.html', response)

def see(request):
    if request.method == "POST":
        pk = request.POST['pk']
        sched = jadwal.objects.get(pk=pk)
        response = {"sched":sched}
        return render(request, 'isi.html', response)
