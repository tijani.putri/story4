from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('biodata/', views.biodata, name='biodata'),
    path('interest/', views.interest, name='interest'),
    path('education/', views.education, name='education'),
    path('experiences/', views.experiences, name='experiences'),
    path('schedule/', views.schedule, name='schedule'),
    path('isi/', views.isi, name='isi'),
]